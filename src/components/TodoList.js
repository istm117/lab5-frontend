import React from 'react';

import TodoItem from './TodoItem'

export default class TodoList extends React.Component {
    render() {
        const todos = this.props.todos;
        const mappedTodos = todos.todos.map((todo, index) =>
            <TodoItem key = {index} todo = {todo}/>
        );
        return (
            <div>
                <div>Todo count: {mappedTodos.length}</div>
                <br/>
                <div>{mappedTodos}</div>
            </div>
        );
    }
}
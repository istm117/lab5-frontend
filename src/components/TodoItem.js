import React from 'react';

export default class TodoItem extends React.Component {
    render() {
        const todo = this.props.todo;
        return (
            <div>
                <div>{todo.title}</div>
                <div>{todo.text}</div>
                <br/>
            </div>
        );
    }
}
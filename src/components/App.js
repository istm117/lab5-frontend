import React from 'react';
import { connect } from 'react-redux'

import { fetchTodos } from '../actions/todoActions';
import TodoList from './TodoList'

@connect((store) => {
    return {
        todos: store.todos
    }
})

export default class App extends React.Component {
    constructor(props) {
        super(props)
    }
    componentWillMount() {
        this.props.dispatch(fetchTodos());
    }
    render() {
        const todos = this.props.todos;
        return (
            <div>
                <div>
                    <div>Header</div>
                </div>
                <br/>
                <div>
                    <div>Menu</div>
                    <div>
                        <div>List</div>
                        <div>Add item</div>
                    </div>
                </div>
                <br/>
                <TodoList todos = {todos}/>
            </div>
        );
    }
}
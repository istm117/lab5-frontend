export default function todoReducer(state = {
    todos: [],
    fetching: false,
    fetched: false,
    error: null
}, action) {
    switch (action.type) {
        case "FETCH_TODOS": {
            return {...state, fetching: true}
        }
        case "FETCH_TODOS_REJECTED": {
            return {...state, fetching: false, error: action.payload}
        }
        case "FETCH_TODOS_FULLFILLED": {
            return {
                ...state,
                fetching: false,
                fetched: true,
                todos: action.payload
            }
        }
        case "GET_TODOS": {
            return {
                ...state,
                todos: action.payload
            }
        }
        default:
            return {
                ...state
            }
    }
}
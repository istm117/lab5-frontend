import axios from 'axios'

export function fetchTodos() {
    return function (dispatch) {
        axios.request({
            method: 'get',
            url: 'http://localhost:3001/getTodos',
        })
        .then((response) => {
            dispatch({
                type: "FETCH_TODOS_FULLFILLED",
                payload: response.data
            })
        })
        .catch((err) => {
            dispatch({
                type: "FETCH_TODOS_REJECTED",
                payload: err
            })
        })
    }
}

export function getTodos() {
    return function (dispatch) {
        dispatch({
            type: "GET_TODOS",
            payload: [
                    {
                        id: "1"
                    }
            ]
        });
    }
}